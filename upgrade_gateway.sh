#!/bin/bash


# Update parameters. Feel free to change this!
FILENAME=piavita_gateway_v3.01
DEVICESW=2.0.1




##########################################################
# WARNING: BE AWARE of changing everythin after this line!
# Upgrading script
# Check if there is a ID.txt file
echo go to home
cd /home/pi
UPDATESERVER=$(sed -n 's|.*"updateserver":"\([^"]*\)".*|\1|p' ID.json)




# Get gateway id stored
#GATEWAY_ID=$(cat /home/pi/ID.txt | grep -oh 'gatewayid":"[0-9]\+' | grep -o '[0-9]\+')

# Download new software upgrade pack
cd /home/pi/tmp

# check if software upgrade exists and if the old is aready finished
[ -d $FILENAME ] && [ -f fertig.txt ] && exit 0
cd /home/pi/tmp
#remove fertig file becaus the update is running to make sure it will run again if it was intrerupted
sudo rm fertig.txt
# Pre-requirements


cd /home/pi
# Check msgpack-lite node is installed
sudo npm install --save msgpack-lite
# Check ethtool is installed
[[ ! $(dpkg-query -W -f='${Status} ${Version}\n' ethtool) == *installed* ]] && sudo apt -y install ethtool
# Check rsync is installed
[[ ! $(dpkg-query -W -f='${Status} ${Version}\n' rsync) == *installed* ]] && sudo apt -y install rsync
echo lade
cd /home/pi/tmp
wget https://bitbucket.org/MMMMarc/piavitagateway/raw/$UPDATESERVER/releases/$FILENAME.zip
unzip $FILENAME
echo go rsync
[ -d $FILENAME ] && echo dir exist || exit 0
# update the ID.json file with new sw
cd $FILENAME && rsync -avzh ./ /home/pi/

#rsync -avzh ./ /home/pi/

cd /home/pi/tmp
[ -d $FILENAME ] && cd /home/pi && sudo node updateID.js $FILENAME $DEVICESW

# check if software upgrade exists and if the old is aready finished
cd /home/pi/tmp
[ -d $FILENAME ] && sudo touch fertig.txt
