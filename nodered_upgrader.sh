#!/bin/bash
cd /home/pi/
sudo usb_modeswitch -v 12d1 -p 1f01 -I -M '55534243123456780000000000000011062000000100000000000000000000'
APN=$(sed -n 's|.*"APN":"\([^"]*\)".*|"\1"|p' /home/pi/ID.json)
UPDATESERVER=$(sed -n 's|.*"updateserver":"\([^"]*\)".*|\1|p' ID.json)
sudo cat wvdial.sample | sed s/@APN@/"$APN"/g  > wvdial.conf && sudo mv wvdial.conf /etc/
sudo systemctl stop wpa_supplicant.service
sudo systemctl disable wpa_supplicant.service
sudo ifdown wlan0
sudo hostapd /etc/hostapd/hostapd.conf &
if [ $(cat /sys/class/net/eth0/carrier) == 0 ];then
  sudo /home/pi/3gstick.sh
  sleep 15
fi


cd /home/pi/
[ ! -d tmp ] && mkdir tmp && chown pi:pi tmp
cd tmp

[ -f upgrade_gateway.sh ] && sudo rm upgrade_gateway.sh

wget https://bitbucket.org/MMMMarc/piavitagateway/raw/"$UPDATESERVER"/upgrade_gateway.sh

chown pi:pi upgrade_gateway.sh && chmod +x upgrade_gateway.sh && sudo -u pi ./upgrade_gateway.sh

sudo node-red-start
